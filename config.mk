ifeq ($(TARGET_GAPPS_ARCH),)
$(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm)
ifneq ($(TARGET_GAPPS_ARCH),arm64)
$(error "GAPPS: Only arm and arm64 are allowed")
endif
endif

TARGET_MINIMAL_APPS ?= false

$(call inherit-product, vendor/gapps/common-blobs.mk)

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.media.effects \
    com.google.widevine.software.drm

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    com.google.android.dialer.support
endif

# app
PRODUCT_PACKAGES += \
    FaceLock \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    PrebuiltBugle \

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    PrebuiltBugle
endif

ifeq ($(TARGET_MINIMAL_APPS),false)

PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt
endif

# priv-app
PRODUCT_PACKAGES += \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleRestore \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCorePi \
    SetupWizard \
    StorageManagerGoogle \
    Turbo

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    GoogleDialer
endif

ifeq ($(TARGET_MINIMAL_APPS),false)
PRODUCT_PACKAGES += \
    Velvet
endif
